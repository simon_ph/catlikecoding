/* I will commit this comment@! */

package application;

import utilities.DateTime;

/* This class represents a single car */
public class Car {
	
	// Attributes of a car
	private String regNo;
	private String make;
	private String model;
	private String driverName;
	private int passengerCapacity;
	private boolean avaliable;
	//private currentBookings[];
	//private pastBookings[];
	
	public Car(String regNo, String make, String model,
			String driverName, int passengerCapacity) {
		this.regNo = regNo;
		this.make = make;
		this.model = model;
		this.driverName = driverName;
		
		// Business Rules
		// #1
		
		// #2
		
		// #3
		if (passengerCapacity < 0) {
			this.passengerCapacity = 1;
		} else if (passengerCapacity > 10) {
			this.passengerCapacity = 10;
		} else {
			this.passengerCapacity = passengerCapacity;
		}
		
		
	}
	
	/* Method is responsible for updating the state of the car object to indicate
	 if it is currently booked or available for booking */
	public boolean book(String firstName, String lastName,
			DateTime required, int numPassengers) {
		
		return true;
	}
	
	/* Get all details of a Car object 
	public String getDetails() {
		return 
	} 
	*/
	
	/* Builds a string and returns it to the calling method
	public String toString() {
		return
	}
	
	 */
	
}
